package main

import (
	"fmt"
	"net/http"
	"net/url"
	"io"
	"log"
)

func main() {

	myUrl, _ := url.Parse("http://192.168.0.7:8092/cebola")

	res, err := http.Get("http://192.168.0.7:8092/cebola")
	if err != nil {
		log.Fatal(err)
	}
	res.Header.Add("Content-Type", "application/json")
	
	request, err := CreateRequest("GET", res.Header, myUrl, res.Body)
	
	// se você implementar corretamente essa linha vai printar a request criada
	PrintRequest(request, err)
}

// função para criar uma request na variável 'req' e popular um erro caso exista na variável 'err'
func CreateRequest(method string, header http.Header, url *url.URL, body io.ReadCloser) (req *http.Request, err error) {
	
	urlS := url.String()

	// faça aqui o código que cria uma request
	r, err := http.NewRequest(method, urlS, body)
	r.Header = header
	return r, err
}

// função para imprimar uma request
func PrintRequest(req *http.Request, err error) {
	
	// faça aqui o código que imprima as informações da request
	// utilize a biblioteca fmt
	fmt.Println(req.URL.String())
	fmt.Println(req.Method)
	fmt.Println(req.Header)
	fmt.Println(err)
}

