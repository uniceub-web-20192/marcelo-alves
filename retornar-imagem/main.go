//Help source: https://stackoverflow.com/questions/47115482/how-do-you-insert-an-image-into-a-html-golang-file
package main


import ( 
		"time"
		"net/http"
		"fmt"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "192.168.0.7:8092",
			IdleTimeout: duration,
		//	Handler    : 
		}

	http.HandleFunc("/", metodoEUrl)
	http.HandleFunc("/cebola", cebolas)
	http.Handle("/imagens/", http.StripPrefix("/imagens/", http.FileServer(http.Dir("./imagens"))))

	server.ListenAndServe()
}

func cebolas(w http.ResponseWriter, r *http.Request) {
	
	fmt.Fprintf(w, "<h1>Cebolas!</h1>")
    fmt.Fprintf(w, "<img src='imagens/cebolas.jpg' alt='cebolas' style='width:1280px;height:720px;'>")
}
